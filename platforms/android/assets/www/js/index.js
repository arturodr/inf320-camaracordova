document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    $('#btn_foto').click(function() {
        navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
            destinationType: Camera.DestinationType.DATA_URL
                                                       });
    });

}
                         
function onSuccess(imageData) {
    $("#foto").attr("src","data:image/jpeg;base64," + imageData);
    
    var urlAPI = "https://api.projectoxford.ai/face/v0/detections?analyzesAge=true&subscription-key=5264c3aefa094ba4808f236f191cc02d"
    //console.log("data:image/jpeg;base64," + imageData);
    
    $.ajax({
        url: urlAPI,
        type: 'POST',
        contentType: 'application/octet-stream',
        processData: false,
        data: dataURItoBlob("data:image/jpeg;base64," + imageData),  
    })
    .done(function(data) {
        console.log(data);
        if(typeof data !== 'undefined' && data.length > 0){
            var edad = data[0].attributes.age;
            $('#titulo').text("Tienes "+edad+"?");
        } else {
            $('#titulo').text("No se detectó ninguna cara :(");
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        alert(errorThrown+ "-" +textStatus);
    });
}

function onFail(message) {
    alert('Error: ' + message);
}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}